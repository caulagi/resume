# Resume

Based on [https://github.com/afriggeri/cv](https://github.com/afriggeri/cv).
Best to install ```texlive-full``` from apt packages before running as

```
$ xelatex ppc.tex (need to run twice)
```
