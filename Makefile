CC = tectonic
FILENAME = ppc.tex

build:
	$(CC) $(FILENAME)

clean:
	rm -rf *.pdf *.aux *.log *.out
